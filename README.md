#  Centralized Workflow

The Centralized Workflow uses a central repository to serve as the single point-of-entry for all changes to the project. The default development branch is called master and all changes are committed into this branch.

This workflow doesn’t require any other branches besides master. The central repository is *the* official project and should be treated as sacred and immutable (i.e. no rewriting history).

## How it works
- Clone the repository (i.e. git clone).
- Make, stage, and commit changes. (i.e. git add, git commit).
- Merge in the latest from the central repository (i.e. git pull).
- Use git pull –rebase to move all of your commits to the tip of the history.
- This will allow you to handle any conflicts on a case by case basis.
- Push changes (i.e. git push).

This is a easy to understand workflow, great for single contributor and/or small teams or projects (i.e. few contributors). But, resolving conflicts can be a burden as the team scales up.